# Minecraft Vanilla
This Yunohost package will make you able to install a PaperMc Minecraft server on YunoHost.

## Requirements
* A root SSH access to your server
* A server based on Debian

## Installtion

```bash
sudo yunohost app install https://gitea.com/Ace_Pendragon/PaperMC-1.19.4.git
sudo yunohost firewall allow TCP 25565
sudo yunohost firewall allow TCP 25575
```

## Manage your server

* `systemctl start server` : Start the server
* `systemctl stop server` : Stop the server
* `systemctl restart server` : Restart the server
* `systemctl enable server` : Makes the server auto-start when booting (default)
* `systemctl disable server` : Do the opposite. 
* `systemctl status server` : Check the status of the app

## Uninstall
### On YunoHost

```bash
sudo yunohost app remove server
```
